<?php
	$sites = array();
	$sites['facebook']   = array('name' => 'Facebook', 		'url' => 'http://facebook.com/sharer.php?u=%URL%&t=%TITLE%');
	$sites['twitter']    = array('name' => 'Twitter', 		'url' => 'http://twitter.com/home?status=%TITLE%+%URL%');
	$sites['googlebuzz'] = array('name' => 'Google Buzz', 	'url' => 'http://www.google.com/buzz/post?url=%URL%&message=%TITLE%');
	$sites['myspace']    = array('name' => 'Myspace', 		'url' => 'http://www.myspace.com/Modules/PostTo/Pages/default.aspx?u=%URL%&c=%TITLE%');
	$sites['msnlive']    = array('name' => 'MSN Live', 		'url' => 'http://profile.live.com/badge/?url=%URL%&title=%TITLE%&description=%DESC%');
	$sites['yahoo']      = array('name' => 'Yahoo', 		'url' => 'http://bookmarks.yahoo.com/toolbar/savebm?opener=tb&u=%URL%&t=%TITLE%&d=%DESC%');
	$sites['linkedin']   = array('name' => 'LinkedIn', 		'url' => 'http://www.linkedin.com/shareArticle?url=%URL%&mini=true&title=%TITLE%&ro=false&summary=%DESC%&source=');
	$sites['orkut']      = array('name' => 'Orkut', 		'url' => 'http://promote.orkut.com/preview?nt=orkut.com&tt=%TITLE%&du=%URL%&cn=%DESC%');