<?php
function network_links_admin_network_settings_submit($form, &$form_state) {

  variable_set('network_link', $_POST );

}

function network_links_admin_network_settings() {
  include_once dirname(__FILE__) . '/nconf.php';
  $netcustom = variable_get('network_link', array());
  $netcustom = $netcustom['network_links'];
  $nets   = $sites;


  $form = array();
  $form['network_links'] = array(
      '#type'   => 'fieldset',
      '#title'  => t('Networks'),
      '#tree'   => TRUE,
      '#theme'  => 'network_list'
    );
    foreach ($nets as $knet => $net) {

      $form['network_links'][$knet]['enable'] = array(
        '#type'   => 'checkbox',
        '#value'	=> $netcustom[$knet]['enable']
      );
      //$dname = $netcustom[$knet]['mname']?$netcustom[$knet]['mname']:$knet;
      $form['network_links'][$knet]['mname'] = array(
        '#type'   => 'item',
        '#value'  => $knet,
      );

      $dname = $netcustom[$knet]['name']?$netcustom[$knet]['name']:$net['name'];
      $form['network_links'][$knet]['name'] = array(
        '#type'   => 'textfield',
        '#default_value'  => $dname,
        '#size'   => 25,
      );
      $api = $netcustom[$knet]['url']?$netcustom[$knet]['url']:$net['url'];
      $form['network_links'][$knet]['url'] = array(
        '#type'   => 'textfield',
        '#default_value'  => $api,
        '#size'   => 95,
      );

    }
    $form['submit'] = array(
      '#type'   => 'submit',
      '#value'  => t('Save')
    );
    return $form;
}

function network_links_admin_settings_submit($form, &$form_state) {
  variable_set('network_links_setting', $form_state['values']);
  drupal_set_message(t('Setting saved'));

}

function network_links_admin_settings() {

  $social_nets = _network_links_get_networks('def');

  $setting  = variable_get('network_links_setting', array());
  $nets   = $setting['network_links']['network_links_nets']?$setting['network_links']['network_links_nets']:array();
  $form['network_links'] = array('#tree' => TRUE);

  $desc = count($social_nets) ? '' : t('You will need enable some networks first, please configure the networks !link', array('!link' => l(t('Configure'), 'admin/settings/network-links/networks')));
  $form['network_links']['network_links_nets'] = array(
      '#type'          => 'checkboxes',
      '#title'         => t('Enabled share links'),
      '#description'   => t('Choose which websites you would like to enable a share link for.') . '<br>' . $desc,
      '#default_value' => $nets,
      '#options'       => $social_nets,
  );

  $desc   = $setting['network_links']['network_links_nets']['network_share_max_desc_length']?$setting['network_links']['network_links_nets']['network_share_max_desc_length']:50;

  $form['network_links']['network_share_max_desc_length'] = array(
      '#type'         => 'textfield',
      '#maxlength'    => 3,
      '#size'         => 3,
      '#description'  => t('Define the maximum description length passed through the link. Anything over 100 is excessive.'),
      '#title'        => t('Maximum description length'),
      '#default_value'    => $desc,
  );

  $node_types = node_get_types();

  $form['network_links']['nField'] = array(
      '#type'   => 'fieldset',
      '#title'  => t('Setting For Nodes'),
      '#tree'   => TRUE
  );
  foreach ($node_types as $type) {
    $form['network_links']['nField'][$type->type] = array(
        '#type'   => 'fieldset',
        '#title'  => t('@type nodes', array('@type' => ucwords($type->type))),
    );

    $en = $setting['network_links']['nField'][$type->type]['enable']?$setting['network_links']['nField'][$type->type]['enable']:array();
    $form['network_links']['nField'][$type->type]['enable'] = array(
      '#type'   => 'checkbox',
      '#title'  => t('Enable'),
      '#default_value' => $en,
    );

    $en = $setting['network_links']['nField'][$type->type]['showin']?$setting['network_links']['nField'][$type->type]['showin']:array();
    $form['network_links']['nField'][$type->type]['showin'] = array(
      '#type'   => 'checkboxes',
      '#title'  => 'show in',
      '#options'  => array(
        'teaser'  => t('Teaser'),
        'full'    => t('Full Node'),
      ),
      '#default_value' => $en,
    );

    $weight = $setting['network_links']['nField'][$type->type]['network_links_weight']?$setting['network_links']['nField'][$type->type]['network_links_weight']:FALSE;
    $form['network_links']['nField'][$type->type]['network_links_weight'] = array(
        '#type'          => 'weight',
        '#delta'         => 10,
        '#description'   => t('Where you want the share links to appear in the node. Negative numbers are rendered earlier, positive numbers are rendered later.'),
        '#title'         => t('Weight'),
        '#default_value' => $weight,
    );
  }

  $form['submit'] = array(
    '#type'   => 'submit',
    '#value'  => t('Save')
  );
  return $form;

}

function network_links_networks_admin_settings() {
  $form = array();

  return system_settings_form($form);
}