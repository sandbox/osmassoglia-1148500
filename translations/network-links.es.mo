��          �      ,      �  6   �  ^   �  !        A     X  r   l     �  
   �               8     >     O     d     q     ~  W  �  :   �  _     .   x     �     �  �   �     �     �  $   �  '   �     �               ;     J     Y     	                                                        
                 Choose which node types to display the share links on. Define the maximum description length passed through the link. Anything over 100 is excessive. Disable social link for this node Display in teaser view Enabled share links If enabled, the share links will appear in node teasers. If disabled, they will only appear on the full node page. Maximum description length Node types Setting for Social Links Settings for Social Links Share Share with @name Social Link settings Social Links Social links Weight Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2011-04-12 13:36-0500
PO-Revision-Date: 2011-04-13 17:29-0300
Last-Translator: oscar sanhueza <osmassoglia@gmail.com>
Language-Team: Spanish <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
 Seleccione que tipos de nodos mostraran los links sociales Define el tamaño maximo pasado atraves del link. Cualquier tamaño superior a 100 es excesivo. Deshabilitar los links sociales para este nodo Monstrar en modo resumen Habilitar los links sociales Si habilitas esta opción, los links sociales apareceran en la version resumen de los nodos. SI esta desabilitado los links sociales se mostraran en el modo full. Tamaño de descripcion  maxima Tipo de nodo Configuración para los social links Configuraciones para los links sociales Compártelo en Compartirlo en @name Links sociales configuración Links sociales Links sociales Peso 